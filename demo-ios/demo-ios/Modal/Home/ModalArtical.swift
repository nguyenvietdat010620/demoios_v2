//
//  ModalArtical.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 13/11/2023.
//

import Foundation
struct ModalArtical: Codable {
    var id: Int
    var category_id: Int
    var title: String
    var slug: String
    var picture: String
    var picture_caption: String?
    var created_at: String
    var category_name: String
    var link: String
    
//    init(id: Int, category_id: Int, title: String, slug: String, picture: String, picture_caption: String, created_at: String, category_name: String, link: Int) {
//        self.id = id
//        self.category_id = category_id
//        self.title = title
//        self.slug = slug
//        self.picture = picture
//        self.picture_caption = picture_caption
//        self.created_at = created_at
//        self.category_name = category_name
//        self.link = link
//    }
}
