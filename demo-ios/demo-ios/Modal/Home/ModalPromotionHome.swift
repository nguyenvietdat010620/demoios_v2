//
//  ModalPromotionHome.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 13/11/2023.
//

import Foundation
struct PromotionHome: Codable {
    var id: Int
    var category_id: Int
    var code: String
    var name: String
    var slug: String
    var content: String
    var picture: String
    var from_date: String
    var to_date: String
    var amount: Int
    var type: Int
    var kind:Int
    var created_at: String
    var category_name: String
    var amount_text: String
    var link: String
    var is_bookmark: Bool
    
}
