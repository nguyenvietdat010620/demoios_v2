//
//  ModalHome.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 13/11/2023.
//

import Foundation

// MARK: - Welcome
struct Resp: Codable {
    let status: Int
    let code: Int
    let message: String
    let data: Modalhome?
}

struct Modalhome : Codable {
    var articleList: [ModalArtical]
    var promotionList: [PromotionHome]
    var doctorList: [ModalDoctorHome]
    
}

