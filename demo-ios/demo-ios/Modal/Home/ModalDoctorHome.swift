//
//  ModalDoctorHome.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 08/11/2023.
//

import Foundation
struct ModalDoctorHome: Codable{
    
    var id: Int
    var full_name: String
    var name: String
    var last_name: String
    var contact_email: String
    var phone: String
    var avatar: String?
    var majors_name: String
    var ratio_star: Float
    var number_of_reviews: Int
    var number_of_stars:Int
    
}

