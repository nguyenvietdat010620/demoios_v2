//
//  HomeTVViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 31/10/2023.
//

import UIKit
import Kingfisher

class HomeTVViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    var listImage = ["https://img-zlr1.tv360.vn/image1/2022/12/15/16/1671097802869/d0ccb5096c10_640_360.jpg",
                     "https://img-zlr1.tv360.vn/image1/2022/08/30/16/6a0879b0/6a0879b0-3a55-40b8-a136-15dd4b24c3d4_640_360.jpg",
                     "https://img-zlr1.tv360.vn/image1/2022/09/28/17/57c153ae/57c153ae-84eb-42c8-aa4d-2d4bbe135420_640_360.jpg",
                     "https://img-zlr1.tv360.vn/image1/2022/08/30/22/44483411/44483411-94e9-48b8-8d0e-121a78e8cd05_640_360.jpg",
                     "https://img-zlr1.tv360.vn/image1/2022/09/23/11/4d903cf2/4d903cf2-cb6c-4d5e-839b-208bbe7d8623_640_360.jpg"
    ]
    @IBOutlet weak var listBanner: UICollectionView!
    
    @IBOutlet weak var viewBanner: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "BannerCollectionViewCell", bundle: nil)
        listBanner.register(nib, forCellWithReuseIdentifier: "BannerCollectionViewCell")
        listBanner.delegate = self
        listBanner.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listImage.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath as IndexPath) as! BannerCollectionViewCell
        let linkimag = URL(string: listImage[indexPath.row])
        
        let processor = DownsamplingImageProcessor(size: CGSize(width: 600, height: 600))
        |> RoundCornerImageProcessor(cornerRadius: 0)
        cell.imageView.kf.indicatorType = .activity
        cell.imageView.kf.setImage(
            with: linkimag,
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
//                cell.imageView.frame.size = CGSize(width: 600, height: 300)
                cell.imageView.image =  cell.imageView.image
                
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 500, height: 500)
    }
    
}

