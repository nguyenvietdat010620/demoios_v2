//
//  CodeViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 07/11/2023.
//

import UIKit
import KeychainSwift
struct focusTextfield{
    var isFocus:Bool
    var didFocus: Bool
    var stringText: String!
}
protocol screenSignDelegate {
    func delege(data: String)
}

class CodeViewController: UIViewController, UITextFieldDelegate  {
    var delegatePro: screenSignDelegate?
    let keychain = KeychainSwift()
    @IBOutlet weak var buttonContinue: UIButton!
    @IBOutlet weak var textfield6: UITextField!
    @IBOutlet weak var textfield5: UITextField!
    @IBOutlet weak var textfield4: UITextField!
    @IBOutlet weak var textfield3: UITextField!
    @IBOutlet weak var textfield2: UITextField!
    @IBOutlet weak var textfield1: UITextField!
    @IBOutlet weak var space4: NSLayoutConstraint!
    @IBOutlet weak var space3: NSLayoutConstraint!
    @IBOutlet weak var space2: NSLayoutConstraint!
    @IBOutlet weak var space1: NSLayoutConstraint!
    @IBOutlet weak var labelConfirmPhoneNumber: UILabel!
    
    var phoneNumber: String = ""
    let screenSize = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var codePin = keychain.get("codePin") ?? ""
       
        buttonContinue.layer.cornerRadius = 24
        buttonContinue.titleLabel?.textColor = .white
        buttonContinue.backgroundColor = UIColor(red: 44/255, green: 134/256, blue: 103/256, alpha: 0.3)
        
        stringProcessSetup()
        delegateTextField()
        textfield1.becomeFirstResponder()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupSpace()
    }
    
    func delegateTextField(){
        textfield1.delegate = self
        textfield2.delegate = self
        textfield3.delegate = self
        textfield4.delegate = self
        textfield5.delegate = self
        textfield6.delegate = self
        textfield1.keyboardType = .numberPad
        textfield2.keyboardType = .numberPad
        textfield3.keyboardType = .numberPad
        textfield4.keyboardType = .numberPad
        textfield5.keyboardType = .numberPad
        textfield6.keyboardType = .numberPad
        textfield1.setLeftPaddingPoints(10)
        textfield2.setLeftPaddingPoints(10)
        textfield3.setLeftPaddingPoints(10)
        textfield4.setLeftPaddingPoints(10)
        textfield5.setLeftPaddingPoints(10)
        textfield6.setLeftPaddingPoints(10)
    }
    
    func saveCodeToKeyChain() {
        keychain.set("11111", forKey: "codePin")
    }
    
    
    @IBAction func actionContinue(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
    
        vc.modalPresentationStyle = .fullScreen
        saveCodeToKeyChain()
        self.present(vc, animated: false)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 1
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =  currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        nextFocus()
        checkEnableNextButton()
    }
    
    func checkEnableNextButton() {
        let text1 = textfield1?.text ?? ""
        let text2 = textfield2?.text ?? ""
        let text3 = textfield3?.text ?? ""
        let text4 = textfield4?.text ?? ""
        let text5 = textfield5?.text ?? ""
        let text6 = textfield6?.text ?? ""
        if(!text1.isEmpty && !text2.isEmpty && !text3.isEmpty && !text4.isEmpty && !text5.isEmpty && !text6.isEmpty ){
            buttonContinue.backgroundColor = UIColor(red: 44/255, green: 134/256, blue: 103/256, alpha: 1)
        }
    }
    
    func nextFocus() {
        var listtextField = [textfield1, textfield2,textfield3,textfield4,textfield5,textfield6]
        
        for (index, itemT) in listtextField.enumerated() {
            let text = itemT?.text ?? ""
            if(!text.isEmpty){
                switch index {
                case 0:
                    textfield2.becomeFirstResponder()
                case 1:
                    textfield3.becomeFirstResponder()
                case 2:
                    textfield4.becomeFirstResponder()
                case 3:
                    textfield5.becomeFirstResponder()
                case 4:
                    textfield6.becomeFirstResponder()
                default:
                    print()
                }
            }
        }
    }
    
    func stringProcessSetup(){
        let boldText = phoneNumber
        let normalText = "Vui lòng nhập mã gồm 4 chữ số đã được gửi đến bạn vào số điện thoại "
        let normalString = NSMutableAttributedString(string:normalText)
        let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)]
        let attributedString = NSMutableAttributedString(string:"+84 \(boldText)", attributes:attrs)
        normalString.append(attributedString)
        print("Phone \(phoneNumber)")
        labelConfirmPhoneNumber.attributedText = normalString
    }
    
    func setupSpace(){
        space1.constant = (screenSize.width - (40*8))/5
        space2.constant = (screenSize.width - (40*8))/5
        space3.constant = (screenSize.width - (40*8))/5
        space4.constant = (screenSize.width - (40*8))/5
    }
    
    
    @IBAction func goBack(_ sender: Any) {
        
        delegatePro?.delege(data: "dat oke")
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
