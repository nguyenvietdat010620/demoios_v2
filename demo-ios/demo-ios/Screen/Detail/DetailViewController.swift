//
//  DetailViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 14/11/2023.
//

import UIKit

//import RealmSwift
class DetailViewController: UIViewController {
    var dataDetail: resDetaol?
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        // Do any additional setup after loading the view.
    }

    func getData(){
        DetailService.shared.getDetailService { [weak self] result in
            switch result {
            case .success(let posts):
                self?.dataDetail = posts
            case .failure(let error):
                print(error)
            }
    }

    }
}
