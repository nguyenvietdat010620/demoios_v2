//
//  VideoViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 01/11/2023.
//

import UIKit
import Alamofire
class VideoViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        getData()
        // Do any additional setup after loading the view.
    }
    func getData(){
        let headers: HTTPHeaders = [
            "#Authorization" : "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxODc3OTczNDAiLCJ1c2VySWQiOjE4Nzc5NzM0MCwicHJvZmlsZUlkIjoxODc5ODYyMjMsImR2aSI6MzY1NDU2ODMzLCJjb250ZW50RmlsdGVyIjoiMTAwIiwiZ25hbWUiOiIiLCJpYXQiOjE2OTkyNTY4MDIsImV4cCI6MTY5OTg2MTYwMn0.OZS8-hwCVJlLZ44fVYORMilZ0P_Tb56y0WkEsFwjPFNrJ2f6s4VcIrDW8hc66q_FXg7f4Mn_XtXtsxJXuQEd9A",
//
            "Content-Type": "application/json",
            "deviceid": "wap_lx4230g34f420ns43laskl342sl76",
            "devicetype": "Android",
            "lang": "vi",
            "osapptype": "osapptype",
            "osappversion": "1.3.0",
            "sessionid": "635f4d7a-45f0-4c71-9943-865db7c4252a",
            "zoneid": "1"
        ]
        
        let params: [String:Any] = [
            "offset": 0,
                "limit": "10",
                "symbol": "MSFT",
                "datatype": "json",
                "output_size": "compact"
        ]
         AF.request("https://api.tv360.vn/public/v1/composite/get-home?offset=0&limit=10",method: .get, parameters: params ,headers: headers)
            .responseJSON { response in
                debugPrint(response)
            }
    }
    
    
}
