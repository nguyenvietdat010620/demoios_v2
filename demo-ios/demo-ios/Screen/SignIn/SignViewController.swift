//
//  SignViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 31/10/2023.
//

import UIKit


class SignViewController: UIViewController, UITextFieldDelegate,screenSignDelegate {
    
    @IBOutlet weak var buttonChangelanguage: UIButton!
    @IBOutlet weak var buttonContinue: UIButton!
    @IBOutlet weak var labelVung: UILabel!
    @IBOutlet weak var textInput: UITextField!
    @IBOutlet weak var viewInput: UIView!
    @IBOutlet var viewMain: UIView!
    @IBOutlet weak var buttonRight: UIButton!
    
    let name = UserDefaults.standard.string(forKey: "phoneNumberLogin") ?? ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textInput.delegate = self
        viewInput.layer.borderColor = CGColor(red: 44/245, green: 134/256, blue: 103/256, alpha: 1)
        viewInput.layer.borderWidth = 1
        viewInput.layer.cornerRadius = 28
        viewInput.layer.masksToBounds = false
        textInput.keyboardType = .numberPad
        textInput.borderStyle = UITextField.BorderStyle.none
        textInput.placeholder = name.count > 0 ? name :  "Nhập số điện thoại"
        textInput.font = .systemFont(ofSize: 15, weight: .bold)
        labelVung.layer.addBorder(edge: UIRectEdge.right, color: .gray, thickness: 1)
        
        buttonContinue.layer.cornerRadius = 24
        buttonContinue.titleLabel?.textColor = .white
        buttonContinue.backgroundColor = UIColor(red: 44/255, green: 134/256, blue: 103/256, alpha: 0.3)
        buttonRight.layer.cornerRadius = 5
        buttonRight.titleLabel?.font = .systemFont(ofSize: 15, weight: .bold)
        buttonRight.titleLabel?.textColor = .white
        buttonRight.backgroundColor = UIColor(red: 0/255, green: 61/256, blue: 115/256, alpha: 0.3)
        
    }
    // delegate k xoa example
    func delege(data: String) {
        print("data", data)
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textInput.text?.count == 9){
            print(textInput.text?.count)
            buttonContinue.backgroundColor = UIColor(red: 44/255, green: 134/256, blue: 103/256, alpha: 1)
        }
        else{
            buttonContinue.backgroundColor = UIColor(red: 44/255, green: 134/256, blue: 103/256, alpha: 0.3)
        }
        return true
    }
    
    
    
    @IBAction func actionContinue(_ sender: Any) {
        //
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CodeViewController") as! CodeViewController
        vc.delegatePro = self
        //        vc.navigationController?.pushViewController(vc, animated: false)
        vc.phoneNumber = textInput.text!
        
        savePhoneToUserDefaults()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
        
        //        }
    }
    
    func savePhoneToUserDefaults() {
        UserDefaults.standard.set(textInput.text!, forKey: "phoneNumberLogin")
    }
    
}
extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        let border = CALayer()
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
    
}
