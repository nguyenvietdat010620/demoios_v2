//
//  HomeCollectionViewCell.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 14/11/2023.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageInfor: UIImageView!
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var labelInfor: UILabel!
    @IBOutlet weak var hotsale: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
