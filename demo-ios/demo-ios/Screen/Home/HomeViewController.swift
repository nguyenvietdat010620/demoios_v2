//
//  HomeViewController.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 31/10/2023.
//

import UIKit
import Alamofire
import SQLite
import SQLite3
class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var db: OpaquePointer?
    var listPromotion: [PromotionHome] = []
    var listArtcal: [ModalArtical] = []
    var listDoctor: [ModalDoctorHome] = []
    var resp: Resp?
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var labelNameUser: UILabel!
    @IBOutlet weak var statusAction: UIView!
    @IBOutlet weak var buttonAvata: UIButton!
    
    @IBOutlet weak var tableViewHome: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        setupView()
        let nib = UINib(nibName: "HomeTableViewCell", bundle: nil)
        tableViewHome.register(nib, forCellReuseIdentifier: "HomeTableViewCell")
        // Do any additional setup after loading the view.
        tableViewHome.dataSource = self
        tableViewHome.delegate = self
        tableViewHome!.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        tableViewHome.separatorStyle = .none
        
        imagePicker.delegate = self
        
        
    }
    
    
    
    func getData(){
        AF.request("https://gist.githubusercontent.com/hdhuy179/f967ffb777610529b678f0d5c823352a/raw", method: .get)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let data):
                    do {
                        let jsonData = try JSONSerialization.data(withJSONObject: data)
                        self.resp = try JSONDecoder().decode(Resp.self, from: jsonData)
                        self.listArtcal = self.resp?.data?.articleList ?? []
                        self.listPromotion = self.resp?.data?.promotionList ?? []
                        self.listDoctor = self.resp?.data?.doctorList ?? []
                    } catch {
                        print("Error decoding JSON: \(error)")
                    }
                case .failure(let err):
                    print("data" , err)
                default:
                    print("false")
                }
            }
    }
    
    func setupView(){
        statusAction.roundCorners(.allCorners, radius: 4)
        buttonAvata.layer.cornerRadius = 21
    }
    
    
    @IBAction func avataAction(_ sender: Any) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath as IndexPath) as! HomeTableViewCell
        switch indexPath.row {
        case 0:
            cell.listArtcal = listArtcal
            cell.number = listArtcal.count
            cell.labelTitle.text = "Tin tức"
            cell.index = 0
            cell.buttonViewAll.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
            
            
        case 1:
            cell.listPromotion = listPromotion
            cell.number = listPromotion.count
            cell.labelTitle.text = "Khuyến mãi"
            cell.index = 1
            
        case 2:
            cell.listDoctor = listDoctor
            cell.number = listDoctor.count
            cell.labelTitle.text = "Giới thiệu bác sĩ"
            cell.index = 2
            
        default:
            print()
        }
        layoutTextTableView(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 265
    }
    
    
    func layoutTextTableView(cell: HomeTableViewCell){
        cell.labelTitle.font = UIFont(name:"HelveticaNeue-Bold", size: 17)
    }
    
    @IBAction func actionAvata(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let resizedImage = resizeImage(image: selectedImage, targetSize: CGSize(width: 42, height: 42))
                        
            buttonAvata.setImage(resizedImage, for: .normal)
            buttonAvata.layer.cornerRadius = 21
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
            let size = image.size
            let widthRatio = targetSize.width / size.width
            let heightRatio = targetSize.height / size.height

            var newSize: CGSize
            if widthRatio > heightRatio {
                newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
            } else {
                newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
            }

            let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

            UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
            image.draw(in: rect)
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return newImage!
        }
    
    @objc func buttonTapped() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
    }
    
}

extension UIView {
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }}


