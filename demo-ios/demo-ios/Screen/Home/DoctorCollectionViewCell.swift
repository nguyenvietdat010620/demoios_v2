//
//  DoctorCollectionViewCell.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 14/11/2023.
//

import UIKit

class DoctorCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var numberOfreview: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var majorLabel: UILabel!
    @IBOutlet weak var avataDoctor: UIImageView!
    @IBOutlet weak var numberOfStart: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
