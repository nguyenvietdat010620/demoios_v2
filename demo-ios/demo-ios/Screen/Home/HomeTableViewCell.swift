//
//  HomeTableViewCell.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 14/11/2023.
//

import UIKit
import Kingfisher
class HomeTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    var listDoctor: [ModalDoctorHome] = []
    var listPromotion: [PromotionHome] = []
    var listArtcal: [ModalArtical] = []
    var number: Int?
    var index: Int?
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonViewAll: UIButton!
    @IBOutlet weak var collectionViewIntable: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
        let nib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
        collectionViewIntable.register(nib, forCellWithReuseIdentifier: "HomeCollectionViewCell")
        
        let nib2 = UINib(nibName: "DoctorCollectionViewCell", bundle: nil)
        collectionViewIntable.register(nib2, forCellWithReuseIdentifier: "DoctorCollectionViewCell")
        
        collectionViewIntable.dataSource = self
        collectionViewIntable.delegate = self
        
        collectionViewIntable!.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return number ?? 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath as IndexPath) as! HomeCollectionViewCell
        
        let celldoctor = collectionView.dequeueReusableCell(withReuseIdentifier: "DoctorCollectionViewCell", for: indexPath as IndexPath) as! DoctorCollectionViewCell
        switch index {
        case 0:
            let imageURL = !listArtcal[indexPath.row].picture.isEmpty
            ? listArtcal[indexPath.row].picture
            : "https://images.vietnamtourism.gov.vn/vn//images/2021/ho_guom.jpg"
            cell.labelInfor.text = listArtcal[indexPath.row].title
            cell.labelInfor.font = UIFont(name:"HelveticaNeue-Bold", size: 15)
            cell.date.text = listArtcal[indexPath.row].created_at
            cell.date.textColor = UIColor(red: 150/256, green: 155/256, blue: 171/256, alpha: 1)
            cell.date.font = UIFont(name:"HelveticaNeue-Bold", size: 13)
            cell.hotsale.text = "Ưu đãi hot"
            cell.hotsale.textColor = UIColor(red: 44/256, green: 134/256, blue: 103/256, alpha: 1)
            cell.hotsale.font = UIFont(name:"HelveticaNeue-Bold", size: 13)
            ImageLoader.loadImage(from: imageURL) { image in
                DispatchQueue.main.async {
                    cell.imageInfor.image = image
                }
            }
            shadow(cell: cell)
            return cell
        case 1:
            cell.labelInfor.text = listPromotion[indexPath.row].name
            cell.labelInfor.font = UIFont(name:"HelveticaNeue-Bold", size: 15)
            cell.hotsale.text = "Ưu đãi hot"
            cell.hotsale.textColor = UIColor(red: 44/256, green: 134/256, blue: 103/256, alpha: 1)
            cell.hotsale.font = UIFont(name:"HelveticaNeue-Bold", size: 13)
            cell.date.text = listPromotion[indexPath.row].from_date
            cell.date.textColor = UIColor(red: 150/256, green: 155/256, blue: 171/256, alpha: 1)
            cell.date.font = UIFont(name:"HelveticaNeue-Bold", size: 13)
            let imageURL = listPromotion[indexPath.row].picture
            ImageLoader.loadImage(from: imageURL) { image in
                DispatchQueue.main.async {
                    cell.imageInfor.image = image
                }
            }
            shadow(cell: cell)
            return cell
        case 2:
            
            let imageURL = listDoctor[indexPath.row].avatar ?? ""
            if(!imageURL.isEmpty){
                ImageLoader.loadImage(from: imageURL) { image in
                    DispatchQueue.main.async {
                        celldoctor.avataDoctor.image  = image
                    }
                }
            }
            else{
                celldoctor.avataDoctor.image = UIImage(named: "doctor")
            }
            celldoctor.majorLabel.text = listDoctor[indexPath.row].majors_name
            celldoctor.nameLabel.text = listDoctor[indexPath.row].name
            celldoctor.nameLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 15)
            celldoctor.numberOfStart.text = String(listDoctor[indexPath.row].number_of_stars)
            celldoctor.numberOfStart.font = UIFont(name:"HelveticaNeue-Bold", size: 15)
            celldoctor.numberOfreview.font = UIFont(name:"HelveticaNeue-Bold", size: 15)
            celldoctor.numberOfreview.text = "(\(listDoctor[indexPath.row].number_of_reviews))"
            celldoctor.numberOfreview.textColor = UIColor(red: 150/256, green: 155/256, blue: 171/256, alpha: 1)
            shadow(cell: celldoctor)
            return celldoctor
        default:
            print()
            return cell
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func shadow(cell: UICollectionViewCell){
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor(red: 238/256, green: 239/256, blue: 244/256, alpha: 1).cgColor
        cell.layer.cornerRadius = 5
        cell.layer.masksToBounds = false
    }
    
//    @IBAction func actionViewAll(_ sender: Any) {
//        if (index == 0){
//            if let parentVC = self  as? HomeViewController {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
//            vc.modalPresentationStyle = .fullScreen
//                parentVC.present(vc, animated: false)
//            }
//        }
//        if (index == 1){
//            print(index)
//        }
//        if (index == 2){
//            print(index)
//        }
//    }
//    
    
    
}

extension HomeTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(index==2){
            return CGSize(width: 150, height: 200)
        }
        else{
            return CGSize(width: 258, height: 220)
        }
    }
}


class ImageLoader {
    static func loadImage(from urlString: String, completion: @escaping (UIImage?) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completion(nil)
                return
            }
            
            let image = UIImage(data: data)
            completion(image)
        }.resume()
    }
}
