//
//  ArtistSQLite.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 13/11/2023.
//

import Foundation
import SQLite

class Artist{
    static let shared = Artist()
    private let tableArtist = Table("tblArtist")
    
    let id = Expression<Int64>("id")
    let category_id = Expression<Int>("category_id")
    let title = Expression<String>("title")
    let slug = Expression<String>("slug")
    let picture = Expression<String>("picture")
    let picture_caption = Expression<String>("picture_caption")
    let created_at = Expression<String>("created_at")
    let category_name = Expression<String>("category_name")
    let link = Expression<String>("link")
    
    private init() {
        do {
            if let connection = databaseSQLite.shared.connection{
                try connection.run(tableArtist.create(temporary: false, ifNotExists: true, withoutRowid: false, block: { (table) in
                    table.column(self.id, primaryKey: true)
                    table.column(self.category_id)
                    table.column(self.title)
                    table.column(self.slug)
                    table.column(self.picture)
                    table.column(self.picture_caption)
                    table.column(self.created_at)
                    table.column(self.category_name)
                    table.column(self.link)
                }))
                print("create success")
            }
            else{
                
                print("create tableArtist false")
            }
            
        } catch  {
            let err = error as NSError
            print("err data: \(err)")
        }
    }
    
    func insertData(category_id: Int, title: String,slug: String, picture: String, picture_caption: String, created_at: String, category_name: String, link:String  ){
        do {
            let insert = tableArtist.insert(
                self.category_id <- category_id,
                self.title <- title,
                self.slug <- slug,
                self.picture <- picture,
                self.picture_caption <- picture_caption,
                self.created_at <- created_at,
                self.category_name <- category_name,
                self.link <- link
            )
        } catch  {
            let err = error as NSError
            print("err data: \(err)")
        }
    }
}
