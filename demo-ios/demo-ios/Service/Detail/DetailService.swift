//
//  DetailService.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 14/11/2023.
//

import Foundation
import Alamofire

class DetailService:NSObject {
    static let shared: DetailService = DetailService()
    
    func getDetailService(completion: @escaping (Result<resDetaol, Error>) -> Void) {
        AF.request("https://gist.github.com/hdhuy179/ef03ed850ad56f0136fe3c5916b3280b/raw/1", method: .get)
            .responseDecodable(of: resDetaol.self) { response in
                switch response.result {
                case .success(let posts):
                    print("oke post", posts)
                    completion(.success(posts))
                case .failure(let error):
                    completion(.failure(error))
                    print("false post", error)
                }
            }
    }
    
}
