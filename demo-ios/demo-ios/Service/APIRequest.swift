//
//  APIRequest.swift
//  demo-ios
//
//  Created by Nguyen Viet Dat on 31/10/2023.
//

import Foundation
import Alamofire

class APIService:NSObject {
    static let shared: APIService = APIService()
    let headers: HTTPHeaders = [
        "Authorization" : "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjkiLCJ1c2VySWQiOjEyOSwicHJvZmlsZUlkIjo5OSwiZHZpIjoxMTgzNCwiY29udGVudEZpbHRlciI6IjEzIiwiZ25hbWUiOiIiLCJpYXQiOjE2MDQwNTcwMTUsImV4cCI6MTYwNDA1NzMxNX0.-ZJNkJyTtwkmHlyU-lIro5jWjWbzEazEkQZMlwcnobNEL5vjXp6qRstj5wv4ci_BORcM5izLyKv4z5LoRm0MHg",
        "User-Agent": "Mozilla/5.0 (Web0S; Linux/SmartTV) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36 WebAppManager",
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Content-Type": "application/json",
        "deviceid": "wap_lx4230g34f420ns43laskl342sl76",
        "devicetype": "TIZEN",
        "lang": "vi",
        "osapptype": "osapptype",
        "osappversion": "1.3.0",
        "sessionid": "635f4d7a-45f0-4c71-9943-865db7c4252a",
    ]
    
    func getData(){
        print("vo api login")
        AF.request("https://api.tv360.vn/public/v1/composite/get-home?offset=0&limit=10",method: .get ,headers: headers)
            .responseJSON { response in
                debugPrint(response)
            }
        
    }
}



extension Dictionary where Key == String, Value == String {
    func toHeader() -> HTTPHeaders {
        var headers: HTTPHeaders = [:]
        for (key, value) in self  {
            headers.add(name: key, value: value)
        }
        return headers
    }
}

struct Welcome1: Decodable {
    let errorCode: Int
    let message: String
    let data: DataClass1?
    
    enum CodingKeys: String, CodingKey {
        case errorCode
        case message
        case data
    }
}

struct DataClass1: Decodable {
    let accessToken, refreshToken, msisdn: String
    let userId, profileId, expiryDuration: Int
    let contentFilter, contentFilterValue: String
    let needChangePassword: Bool
    let name, urlAvatar: String
    let otpToken: String?
}
